importScripts('https://www.gstatic.com/firebasejs/7.16.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.16.0/firebase-messaging.js');
/*Update this config*/
var config = {
    apiKey: "AIzaSyAW3ciADAxtzSHFbL20gRBxdKBJ6sN7tMY",
    authDomain: "ur-delivery.firebaseapp.com",
    databaseURL: "https://ur-delivery.firebaseio.com",
    projectId: "ur-delivery",
    storageBucket: "ur-delivery.appspot.com",
    messagingSenderId: "859357045797",
    appId: "1:859357045797:web:1d64e21412686cf3648bcc",
    measurementId: "G-4G2LB8Q5BD"
};
firebase.initializeApp(config);

const messaging = firebase.messaging();
messaging.usePublicVapidKey('AAAAyBWr5CU:APA91bHi6T7sRX86gFJENR6Z4KLAdsAJvSnJD94rZqNuYJuMR7_T3csgnikLVZP9MsX43gt1lNd98E1oI_K2Qtkjekp_3MB3vOm9ikJS2it2OSUL0xk-fjkBmq1CCYNi67Bj5qqNBZu3');
messaging.setBackgroundMessageHandler(function (payload) {
    console.log('[firebase-messaging-sw.js] Received background message ', payload);
    // Customize notification here
    const notificationTitle = payload.data.title;
    const notificationOptions = {
        body: payload.data.body,
        icon: '/img/logo.png',
        image: '/img/logo.png'
    };

    return self.registration.showNotification(notificationTitle,
            notificationOptions);
});
// [END background_handler]
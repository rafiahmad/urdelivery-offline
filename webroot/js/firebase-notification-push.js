// Initialize Firebase
/*Update this config*/
var config = {
    apiKey: "AIzaSyAW3ciADAxtzSHFbL20gRBxdKBJ6sN7tMY",
    authDomain: "ur-delivery.firebaseapp.com",
    databaseURL: "https://ur-delivery.firebaseio.com",
    projectId: "ur-delivery",
    storageBucket: "ur-delivery.appspot.com",
    messagingSenderId: "859357045797",
    appId: "1:859357045797:web:1d64e21412686cf3648bcc",
    measurementId: "G-4G2LB8Q5BD"
};
firebase.initializeApp(config);

// Retrieve Firebase Messaging object.
const messaging = firebase.messaging();

messaging.requestPermission()
        .then(function () {
            console.log('Notification permission granted.');
            // TODO(developer): Retrieve an Instance ID token for use with FCM.
            if (isTokenSentToServer()) {
                console.log('Token already saved.');
            } else {
                getRegToken();
            }

        })
        .catch(function (err) {
            console.log('Unable to get permission to notify.', err);
        });

function getRegToken(argument) {
    messaging.getToken()
            .then(function (currentToken) {
                if (currentToken) {
                    saveToken(currentToken);
                    console.log(currentToken);
                    setTokenSentToServer(true);
                } else {
                    console.log('No Instance ID token available. Request permission to generate one.');
                    setTokenSentToServer(false);
                }
            })
            .catch(function (err) {
                console.log('An error occurred while retrieving token. ', err);
                setTokenSentToServer(false);
            });
}

function setTokenSentToServer(sent) {
    window.localStorage.setItem('sentToServer', sent ? 1 : 0);
}

function isTokenSentToServer() {
    return window.localStorage.getItem('sentToServer') == 1;
}

function saveToken(currentToken) {
    $.ajax({
        url: '/api/v1/token/',
        method: 'post',
        data: {
            token: currentToken
        }
    }).done(function (result) {
        console.log(result);
    })
}

messaging.onMessage(function (payload) {
    console.log("Message received. ", payload);
    notificationTitle = payload.data.title;
    notificationOptions = {
        body: payload.data.body,
        icon: payload.data.icon,
        image: payload.data.image
    };
    var notification = new Notification(notificationTitle, notificationOptions);
});
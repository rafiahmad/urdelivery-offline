$(function () {
    //Initialize Select2 Elements
    $('.select2').select2({width: '100%'});
    $('input[datepicker]').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd'
    })
});

function loadRiderModel(order_number, rider_id) {
    $('#order-number').val(order_number);
    $('#rider-id').val(rider_id);
    $('#rider-id').change();
    $('#modal-rider').modal();
}
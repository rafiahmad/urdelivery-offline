<?php

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Core\Configure;

/**
 * Orders Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\RidersTable&\Cake\ORM\Association\BelongsTo $Riders
 *
 * @method \App\Model\Entity\Order get($primaryKey, $options = [])
 * @method \App\Model\Entity\Order newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Order[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Order|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Order saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Order patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Order[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Order findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OrdersTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('orders');
        $this->setDisplayField('order_number');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Riders', [
            'foreignKey' => 'rider_id',
            'className' => 'Users',
            'joinType' => 'LEFT',
        ]);
        $this->belongsTo('Tokens', [
            'foreignKey' => 'user_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmptyString('id', null, 'create');

        $validator
                ->scalar('order_number')
                ->maxLength('order_number', 20)
                ->requirePresence('order_number', 'create')
                ->notEmptyString('order_number')
                ->add('order_number', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
                ->scalar('payment_method')
                ->maxLength('payment_method', 10)
                ->requirePresence('payment_method', 'create')
                ->notEmptyString('payment_method');

        $validator
                ->scalar('customer_name')
                ->maxLength('customer_name', 100)
                ->requirePresence('customer_name', 'create')
                ->notEmptyString('customer_name');

        $validator
                ->scalar('shipping_street')
                ->maxLength('shipping_street', 750)
                ->requirePresence('shipping_street', 'create')
                ->notEmptyString('shipping_street');

        $validator
                ->scalar('pincode')
                ->maxLength('pincode', 10)
                ->requirePresence('pincode', 'create')
                ->notEmptyString('pincode');

        $validator
                ->scalar('city')
                ->maxLength('city', 20)
                ->requirePresence('city', 'create')
                ->notEmptyString('city');

        $validator
                ->scalar('state')
                ->maxLength('state', 20)
                ->requirePresence('state', 'create')
                ->notEmptyString('state');

        $validator
                ->scalar('mobile')
                ->maxLength('mobile', 20)
                ->requirePresence('mobile', 'create')
                ->notEmptyString('mobile');

        $validator
                ->scalar('status')
                ->maxLength('status', 20)
                ->allowEmptyString('status');

        $validator
                ->dateTime('pickup_date')
                ->allowEmptyDateTime('pickup_date');

        $validator
                ->dateTime('delivery_date')
                ->allowEmptyDateTime('delivery_date');

        $validator
                ->dateTime('cancel_date')
                ->allowEmptyDateTime('cancel_date');

        $validator
                ->dateTime('reassign_date')
                ->allowEmptyDateTime('reassign_date');

        $validator
                ->scalar('remarks')
                ->maxLength('remarks', 4294967295)
                ->allowEmptyString('remarks');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->isUnique(['order_number']));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    public function getOrderNumber($client_name = 'URD') {
        $prefix = strtoupper($client_name);
        if (strlen($prefix) > 3) {
            $prefix = substr($prefix, 0, 3);
        }
        $lastOrderNumber = $this->find('all', [
            'fields' => [
                'order_number'
            ],
            'conditions' => [
                'order_number LIKE ' => '%' . $prefix
            ],
            'limit' => 1,
            'order' => 'order_number DESC'
        ]);
        if ($lastOrderNumber->count()) {
            return ((int) substr($lastOrderNumber->first()->order_number, 3, -3)) + 1;
        } else {
            return $prefix . '00001';
        }
    }

    protected function _update($entity, $data) {
        if (array_key_exists('status', $data) && !empty($data['status'])) {
            switch (strtolower($data['status'])) {
                case 'pickup':
                    $data['pickup_date'] = date('Y-m-d h:i:s');
                    $this->sendNotification((array) $entity->user_id + array_values(Configure::read('AdminIds')), "Rider has been pickup order number {$entity->order_number}.");
                    break;
                case 'delivered':
                    $data['delivery_date'] = date('Y-m-d h:i:s');
                    $this->sendNotification((array) $entity->user_id + array_values(Configure::read('AdminIds')), "Rider has been delivered order number {$entity->order_number}.");
                    break;
                case 'cancel':
                    $data['cancel_date'] = date('Y-m-d h:i:s');
                    $this->sendNotification((array) $entity->user_id + array_values(Configure::read('AdminIds')), "Rider has been cancle order number {$entity->order_number}.");
                    break;
                case 'reassign':
                    $data['reassign_date'] = date('Y-m-d h:i:s');
                    $data['delivery_date'] = null;
                    $data['pickup_date'] = null;
                    $data['cancel_date'] = null;
                    $this->sendNotification($entity->rider_id, "Rider has been changed for order number {$entity->order_number}.");
                    $this->sendNotification($entity->rider_id, "Order Number {$entity->order_number} is assigned to you");
                    break;
                default:
                    unset($data['status']);
            }
        }
        return parent::_update($entity, $data);
    }

    public function sendNotification($user_id, $msg = '') {
        $tokens = $this->Tokens->find('list', [
                    'conditions' => [
                        'user_id IN' => $user_id
                    ]
                ])->toArray();
        if (!empty($tokens)) {
            $data = json_encode([
                "registration_ids" => array_values($tokens),
                "notification" => [
                    "title" => $msg,
                ],
                "openURL" => "https://www.google.com"
            ]);
            $url = 'https://fcm.googleapis.com/fcm/send';
            $server_key = 'AAAAyBWr5CU:APA91bHi6T7sRX86gFJENR6Z4KLAdsAJvSnJD94rZqNuYJuMR7_T3csgnikLVZP9MsX43gt1lNd98E1oI_K2Qtkjekp_3MB3vOm9ikJS2it2OSUL0xk-fjkBmq1CCYNi67Bj5qqNBZu3';
            $headers = array(
                'Content-Type:application/json',
                'Authorization:key=' . $server_key
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            var_dump(curl_exec($ch));
            die;
            curl_close($ch);
        }
    }

}

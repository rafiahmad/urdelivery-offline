<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmptyString('id', null, 'create');

        $validator
                ->notEmptyString('role');

        $validator
                ->scalar('name')
                ->maxLength('name', 50)
                ->notEmptyString('name');

        $validator
                ->scalar('address')
                ->maxLength('address', 255)
                ->allowEmptyString('address');

        $validator
                ->integer('mobile')
                ->maxLength('mobile', 20)
                ->notEmptyString('mobile');

        $validator
                ->email('email')
                ->requirePresence('email', 'create')
                ->notEmptyString('email')
                ->add('email', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
                ->scalar('password')
                ->maxLength('password', 255)
                ->requirePresence('password', 'create')
                ->notEmptyString('password');

        $validator
                ->scalar('street_address')
                ->maxLength('street_address', 4294967295)
                ->notEmptyString('street_address');

        $validator
                ->integer('pincode')
                ->maxLength('pincode', 10)
                ->notEmptyString('pincode');

        $validator
                ->scalar('city')
                ->maxLength('city', 20)
                ->notEmptyString('city');

        $validator
                ->scalar('state')
                ->maxLength('state', 20)
                ->notEmptyString('state');

        $validator
                ->boolean('status')
                ->notEmptyString('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }

    public function tokenUpdate($email) {
        $token = \Cake\Utility\Security::hash(date('Y-m-d'));
        $this->updateAll([
            'token' => $token,
            'token_expire' => date('Y-m-d h:i:s', strtotime('+1 Day'))
                ], [
            'email' => $email
        ]);
        return $token;
    }

    public function setAuthToken() {
        if ($this->id) {
            $datasourceEntity = $this->get($this->id);
            $token = bin2hex(openssl_random_pseudo_bytes(16));
            $datasourceEntity->auth_token = $token;
            if ($this->save($datasourceEntity)) {
                return $token;
            }
        }
        return false;
    }

}

<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Order Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $rider_id
 * @property string $order_number
 * @property string $payment_method
 * @property string $customer_name
 * @property string $shipping_street
 * @property string $pincode
 * @property string $city
 * @property string $state
 * @property string $mobile
 * @property string|null $status
 * @property \Cake\I18n\FrozenTime|null $pickup_date
 * @property \Cake\I18n\FrozenTime|null $delivery_date
 * @property \Cake\I18n\FrozenTime|null $cancel_date
 * @property \Cake\I18n\FrozenTime|null $reassign_date
 * @property string|null $remarks
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Rider $rider
 */
class Order extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'rider_id' => true,
        'order_number' => true,
        'payment_method' => true,
        'customer_name' => true,
        'shipping_street' => true,
        'pincode' => true,
        'city' => true,
        'state' => true,
        'mobile' => true,
        'status' => true,
        'pickup_date' => true,
        'delivery_date' => true,
        'cancel_date' => true,
        'reassign_date' => true,
        'remarks' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'rider' => true,
    ];
}

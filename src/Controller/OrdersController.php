<?php

namespace App\Controller;

use App\Controller\AppController;
use DataTables\Controller\DataTablesAjaxRequestTrait;
use Cake\Http\Exception\UnauthorizedException;

/**
 * Orders Controller
 *
 * @property \App\Model\Table\OrdersTable $Orders
 *
 * @method \App\Model\Entity\Order[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class OrdersController extends AppController {

    public $default_condition;

    public function initialize() {
        parent::initialize();
        if (in_array($this->request->getParam('action'), ['index', 'getDataTablesContent'])) {
            $auth_user_id = $this->request->getSession()->read('Auth.User.id');
            $auth_user_role = $this->request->getSession()->read('Auth.User.role');
            $this->loadComponent('DataTables.DataTables');
            if (!empty($auth_user_role) && $auth_user_role != '1') {
                $this->default_condition = [
                    'Orders.user_id' => $auth_user_id
                ];
            }
            $this->default_condition = $this->filterDataCondition($this->default_condition);
            $this->DataTables->createConfig('Orders')
                    ->databaseColumn('Orders.id')
                    ->databaseColumn('Orders.rider_id')
                    ->queryOptions([
                        'conditions' => [
                            $this->default_condition
                        ],
                        'contain' => [
                            'Users', 'Riders'
                        ]
                    ])
                    ->column('Orders.order_number', ['label' => 'Order Number'])
                    ->column('Orders.payment_method', ['label' => 'Payment Method'])
                    ->column('Orders.customer_name', ['label' => 'Customer Name'])
                    ->column('Orders.pincode', ['label' => 'Pincode'])
                    ->column('Orders.city', ['label' => 'City'])
                    ->column('Orders.state', ['label' => 'State'])
                    ->column('Orders.mobile', ['label' => 'Mobile'])
                    ->column('Orders.pickup_date', ['label' => 'Pickup Date'])
                    ->column('Orders.delivery_date', ['label' => 'Delivery Date'])
                    ->column('Orders.cancel_date', ['label' => 'Cancel Status'])
                    ->column('Orders.reassign_date', ['label' => 'Reassign Date'])
                    ->column('Orders.status', ['label' => 'Current Status'])
                    ->column('Orders.remarks', ['label' => 'Remarks'])
                    ->column('Orders.created', ['label' => 'Created'])
                    ->column('Riders.name', ['label' => 'Rider Name'])
                    ->column('actions', ['label' => 'Actions', 'database' => false]);
        }
    }

    public function filterDataCondition($default_condition) {
        $session = $this->request->getSession();

        /* condition for final_status */
        if ($this->request->getData('current_status')) {
            $default_condition['Orders.status'] = $this->request->getData('current_status');
            $session->write('Orders.current_status', $this->request->getData('current_status'));
            $this->set('current_status', $this->request->getData('current_status'));
        } else if ($this->request->getData('current_status') === "") {
            $session->delete('Orders.current_status');
        } else if ($session->read('Orders.current_status')) {
            $default_condition['Orders.status'] = $session->read('Orders.current_status');
            $this->set('current_status', $session->read('Orders.current_status'));
        } else if ($this->request->getQuery('current_status')) {
            $default_condition['Orders.status'] = $this->request->getQuery('current_status');
        }

        /* condition for payment_method */
        if ($this->request->getData('payment_method')) {
            $default_condition['Orders.payment_method'] = $this->request->getData('payment_method');
            $session->write('Orders.payment_method', $this->request->getData('payment_method'));
            $this->set('payment_method', $this->request->getData('payment_method'));
        } else if ($this->request->getData('payment_method') === "") {
            $session->delete('Orders.payment_method');
        } else if ($session->read('Orders.payment_method')) {
            $default_condition['Orders.payment_method'] = $session->read('Orders.payment_method');
            $this->set('payment_method', $session->read('Orders.payment_method'));
        } else if ($this->request->getQuery('payment_method')) {
            $default_condition['Orders.payment_method'] = $this->request->getQuery('payment_method');
        }

        /* condition for client name */
        if ($this->request->getData('client_name')) {
            $default_condition['Orders.user_id'] = $this->request->getData('client_name');
            $session->write('Orders.user_id', $this->request->getData('client_name'));
            $this->set('client_name', $this->request->getData('client_name'));
        } else if ($this->request->getData('client_name') === "") {
            $session->delete('Orders.user_id');
        } else if ($session->read('Orders.user_id')) {
            $default_condition['Orders.user_id'] = $session->read('Orders.user_id');
            $this->set('client_name', $session->read('Orders.user_id'));
        } else if ($this->request->getQuery('client_name')) {
            $default_condition['Orders.user_id'] = $this->request->getQuery('client_name');
        }

        /* condition for rider name */
        if ($this->request->getData('rider_name')) {
            $default_condition['Orders.rider_id'] = $this->request->getData('rider_name');
            $session->write('Orders.rider_id', $this->request->getData('rider_name'));
            $this->set('rider_name', $this->request->getData('rider_name'));
        } else if ($this->request->getData('rider_name') === "") {
            $session->delete('Orders.rider_name');
        } else if ($session->read('Orders.rider_name')) {
            $default_condition['Orders.rider_id'] = $session->read('Orders.rider_name');
            $this->set('rider_name', $session->read('Orders.rider_name'));
        } else if ($this->request->getQuery('rider_name')) {
            $default_condition['Orders.rider_id'] = $this->request->getQuery('rider_name');
        }

        /* condition for order number */
        if ($this->request->getData('order_number')) {
            $default_condition['Orders.order_number IN'] = explode(',', $this->request->getData('order_number'));
            $session->write('Orders.order_number', $this->request->getData('order_number'));
            $this->set('order_number', $this->request->getData('order_number'));
        } else if ($this->request->getData('order_number') === "") {
            $session->delete('Orders.order_number');
        } else if ($session->read('Orders.order_number')) {
            $default_condition['Orders.order_number IN'] = explode(',', $session->read('Orders.order_number'));
            $this->set('order_number', $session->read('Orders.order_number'));
        } else if ($this->request->getQuery('order_number')) {
            $default_condition['Orders.order_number IN'] = explode(',', $this->request->getQuery('order_number'));
        }
        return $default_condition;
    }

    /*
     * User DataTable Ajax Request Trait
     */
    use DataTablesAjaxRequestTrait;

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index() {
        if ($this->request->is('api')) {
            if ($this->Auth->user('role') == '2') {
                $this->default_condition['Orders.user_id'] = $this->Auth->user('id');
            } else if ($this->Auth->user('role') == '3') {
                $this->default_condition['Orders.rider_id'] = $this->Auth->user('id');
            }
            $data = $this->paginate($this->Orders, [
                'conditions' => $this->default_condition,
                'contain' => [
                    'Users'
                ],
                'fields' => [
                    'Orders.order_number',
                    'Orders.payment_method',
                    'Orders.customer_name',
                    'Orders.shipping_street',
                    'Orders.pincode',
                    'Orders.city',
                    'Orders.state',
                    'Orders.mobile',
                    'Orders.pickup_date',
                    'Orders.delivery_date',
                    'Orders.cancel_date',
                    'Orders.reassign_date',
                    'Orders.status',
                    'Orders.remarks',
                    'Users.name',
                    'Users.email',
                    'Users.street_address',
                    'Users.pincode',
                    'Users.city',
                    'Users.state',
                    'Users.mobile',
                ]
            ]);
            $this->set(compact('data'));
        } else {
            $this->DataTables->setViewVars('Orders');
            if ($this->Auth->user('role') === 1) {
                $clients = $this->Orders->Users->find('list', ['conditions' => ['role' => 2]]);
                $riders = $this->Orders->Users->find('list', ['conditions' => ['Users.role' => 3]]);
                $this->set(compact('riders', 'clients'));
            }
        }
    }

    /**
     * View method
     *
     * @param string|null $id Order id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $order = $this->Orders->get($id, [
            'contain' => [],
        ]);

        $this->set('order', $order);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $order = $this->Orders->newEntity();
        if ($this->Auth->user('role') === 1) {
            $clients = $this->Orders->Users->find('list', [
                'conditions' => [
                    'role' => 2
                ]
            ]);
        }
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            if ($this->Auth->user('role') === 1) {
                if ($clients->count()) {
                    $client_id = (int) $this->request->getData('client_name');
                    $clients = $clients->toArray();
                    if (array_key_exists($client_id, $clients)) {
                        $user_id = $this->request->getData('client_name');
                        $client_name = $clients[$client_id];
                    } else {
                        $this->Flash->error(__("Please select client to process upload order."));
                        return;
                    }
                } else {
                    $this->Flash->error(__("You dont have clients to add orders."));
                    return;
                }
            } else {
                $user_id = $this->Auth->user('id');
                $client_name = $this->Auth->user('name');
            }
            $data['user_id'] = $user_id;
            $data['order_number'] = $this->Orders->getOrderNumber($client_name);
            $order = $this->Orders->patchEntity($order, $data);
            if ($this->Orders->save($order)) {
                $this->Flash->success(__('The order has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The order could not be saved. Please, try again.'));
        }
        $this->set(compact('order', 'clients'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function multipleAdd() {
        if ($this->request->is('post')) {
            $user_id = $this->Auth->user('id');
            $add = 0;
            $update = 0;
            foreach ($this->request->input('json_decode', true) as $data) {
                $data['user_id'] = $user_id;
                $isAlreadyExist = $this->Orders->find('all', [
                            'conditions' => [
                                'order_number' => $data['order_number']
                            ]
                        ])->first();
                if ($isAlreadyExist == NULL) {
                    $order = $this->Orders->newEntity();
                    $order = $this->Orders->patchEntity($order, $data);
                    if ($this->Orders->save($order)) {
                        $add++;
                    }
                } else if ($isAlreadyExist->user_id == $user_id) {
                    $isAlreadyExist = $this->Orders->patchEntity($isAlreadyExist, $data);
                    if ($this->Orders->save($isAlreadyExist)) {
                        $update++;
                    }
                }
                $this->Flash->success(__('Total {0} order added and  {1} order updated.', $add, $update));
                return;
            }
            $this->Flash->error(__('The order could not be saved. Please, try again.'));
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Order id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null) {
        $conditions = [];
        if ($this->Auth->user('role') !== 1) {
            $conditions['user_id'] = $this->Auth->user('id');
        }
        $order = $this->Orders->get($id, [
            'contain' => [],
            'conditions' => $conditions
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $order = $this->Orders->patchEntity($order, $this->request->getData());
            if ($this->Orders->save($order)) {
                $this->Flash->success(__('The order has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The order could not be saved. Please, try again.'));
        }
        $this->set(compact('order'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Order id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $conditions = [];
        if ($this->Auth->user('role') !== 1) {
            $conditions['user_id'] = $this->Auth->user('id');
        }
        $order = $this->Orders->get($id, [
            'contain' => [],
            'conditions' => $conditions
        ]);
        if ($this->Orders->delete($order)) {
            $this->Flash->success(__('The order has been deleted.'));
        } else {
            $this->Flash->error(__('The order could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function assignRider() {
        $this->request->allowMethod(['post', 'api']);
        if ($this->Auth->user('role') != '1') {
            throw new UnauthorizedException(__('You are not alowed to access this page'));
        }
        $this->Orders->setPrimaryKey('order_number');
        $order = $this->Orders->get($this->request->getData('order_number'));
        $rider = $this->Orders->Users->findById($this->request->getData('rider_id'));
        if ($rider->count()) {
            $orderData = [
                'rider_id' => $this->request->getData('rider_id')
            ];
            if (is_null($order->rider_id) === false) {
                $orderData['status'] = 'reassign';
            } else {
                $orderData['status'] = 'assign';
            }
            $order = $this->Orders->patchEntity($order, $orderData);
            $this->Orders->save($order);
            $this->Flash->success(__('Order assign successfully.'));
        } else {
            $this->Flash->error(__('Rider not found.'));
        }
        $this->redirect($this->referer());
    }

    public function clearFilterSession() {
        $this->request->getSession()->delete('Orders');
        $this->redirect($this->referer());
    }

    public function updateStatus() {
        $this->request->allowMethod(['post', 'api']);
        if ($this->Auth->user('role') != '3') {
            throw new UnauthorizedException(__('You are not alowed to access this page'));
        }
        $this->Orders->setPrimaryKey('order_number');
        $order = $this->Orders->get($this->request->getData('order_number'));
        $order = $this->Orders->patchEntity($order, ['status' => $this->request->getData('status')]);
        if ($this->Orders->save($order)) {
            $this->Flash->success(__('Order {0} successfully.', strtolower($this->request->getData('status'))));
        } else {
            $this->Flash->error(__('Order not {0} successfully.', strtolower($this->request->getData('status'))));
        }
    }

}

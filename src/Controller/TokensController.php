<?php

namespace App\Controller;

use App\Controller\AppController;

class TokensController extends AppController {

    public function index() {
        $this->request->allowMethod(['post']);
        $isAlreadyExist = $this->Tokens->findByUserId($this->Auth->user('id'))->first();
        $success = false;
        if ($isAlreadyExist == NULL) {
            $token = $this->Tokens->newEntity();
            $token = $this->Tokens->patchEntity($token, ['token' => $this->request->getData('token'), 'user_id' => $this->Auth->user('id')]);
            if ($this->Tokens->save($token)) {
                $success = true;
            }
        } else {
            $isAlreadyExist = $this->Tokens->patchEntity($isAlreadyExist, ['token' => $this->request->getData('token')]);
            if ($this->Tokens->save($isAlreadyExist)) {
                $success = true;
            }
        }
        if ($success) {
            $this->Flash->success(__('The token has been saved.'));
        } else {
            $this->Flash->error(__('The token could not be saved. Please, try again.'));
        }
    }

}

<?php

namespace App\Controller;

use App\Controller\AppController;
use DataTables\Controller\DataTablesAjaxRequestTrait;
use Cake\Http\Exception\UnauthorizedException;

/**
 * Riders Controller
 *
 *
 * @method \App\Model\Entity\Rider[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RidersController extends AppController {

    public function __construct(\Cake\Http\ServerRequest $request = null, \Cake\Http\Response $response = null, $name = null, $eventManager = null, $components = null) {
        parent::__construct($request, $response, $name, $eventManager, $components);
        $this->loadModel('Users');
        $this->Riders = $this->Users;
    }

    public function initialize() {
        parent::initialize();
        $this->loadComponent('DataTables.DataTables');
        $this->DataTables->createConfig('Riders')
                ->table('Users')
                ->databaseColumn('Users.id')
                ->queryOptions([
                    'conditions' => [
                        'Users.role' => '3'
                    ]
                ])
                ->column('Users.name', ['label' => 'Client Name'])
                ->column('Users.email', ['label' => 'Email'])
                ->column('Users.mobile', ['label' => 'Mobile Number'])
                ->column('Users.status', ['label' => 'Status'])
                ->column('actions', ['label' => 'Actions', 'database' => false]);
    }

    /*
     * User DataTable Ajax Request Trait
     */
    use DataTablesAjaxRequestTrait;

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index() {
        if ($this->Auth->user('role') != '1') {
            throw new UnauthorizedException(__('You are not alowed to access this page'));
        }
        if ($this->request->is('api')) {
            $data = $this->paginate($this->Riders, [
                'conditions' => [
                    'Users.role' => '3'
                ]
            ]);
            $this->set(compact('data'));
        } else {
            $this->DataTables->setViewVars('Riders');
        }
    }

    /**
     * View method
     *
     * @param string|null $id Rider id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $rider = $this->Riders->get($id, [
            'contain' => [],
        ]);

        $this->set('rider', $rider);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $rider = $this->Riders->newEntity();
        if ($this->request->is('post')) {
            $rider = $this->Riders->patchEntity($rider, $this->request->getData() + ['role' => '3']);
            if ($this->Riders->save($rider)) {
                $this->Flash->success(__('The rider has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The rider could not be saved. Please, try again.'));
        }
        $this->set(compact('rider'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Rider id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null) {
        $rider = $this->Riders->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            if (empty($data['password'])) {
                unset($data['password']);
            }
            $rider = $this->Riders->patchEntity($rider, $data);
            if ($this->Riders->save($rider)) {
                $this->Flash->success(__('The rider has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The rider could not be saved. Please, try again.'));
        }
        $this->set(compact('rider'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Rider id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $rider = $this->Riders->get($id);
        if ($this->Riders->delete($rider)) {
            $this->Flash->success(__('The rider has been deleted.'));
        } else {
            $this->Flash->error(__('The rider could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

}

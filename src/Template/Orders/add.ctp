<!-- Default box -->
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"><?= __('Add Order') ?></h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
        </div>
    </div>
    <?= $this->Form->create($order) ?>
    <div class="box-body">
        <?php
        if ($authUser['role'] === 1) {
            echo $this->Form->control('client_name', ['options' => $clients, 'required' => true]);
        }
        echo $this->Form->control('payment_method', ['options' => ['cash' => 'Cash', 'prepaid' => 'Prepaid']]);
        echo $this->Form->control('customer_name');
        echo $this->Form->control('shipping_street');
        echo $this->Form->control('pincode');
        echo $this->Form->control('city');
        echo $this->Form->control('state');
        echo $this->Form->control('mobile');
        echo $this->Form->control('remarks');
        ?>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        <?= $this->Form->button(__('Add')) ?>
    </div>
    <?= $this->Form->end() ?>
    <!-- /.box-footer-->
</div>
<!-- /.box -->

<?php

foreach ($results as $result) {
    $this->DataTables->prepareData([
        h($result->order_number),
        h($result->payment_method),
        h($result->customer_name),
        h($result->pincode),
        h($result->city),
        h($result->state),
        h($result->mobile),
        h($result->pickup_date ? $result->pickup_date : 'N/A'),
        h($result->delivery_date ? $result->delivery_date : 'N/A'),
        h($result->cancel_date ? $result->cancel_date : 'N/A'),
        h($result->reassign_date ? $result->reassign_date : 'N/A'),
        h($result->status ? $result->status : 'N/A'),
        h($result->remarks),
        h($result->created),
        h(isset($result['rider']) ? $result['rider']->name : ''),
        ($authUser['role'] === 1 ? $this->Html->link(__('<i class="fa fa-user"></i>'), '#', ['class' => 'btn btn-info', 'title' => 'Assign Rider', 'onclick' => "loadRiderModel('{$result->order_number}','{$result->rider_id}')", 'escape' => false]) . '&nbsp;' : '') .
        $this->Form->postLink(__('<i class="fa fa-trash"></i>'), ['action' => 'delete', $result->id], ['confirm' => __('Are you sure you want to delete # {0}?', $result->order_number), 'class' => 'btn btn-info', 'escape' => false]) . '&nbsp;' .
        $this->Html->link(__('<i class="fa fa-pencil"></i>'), ['action' => 'edit', $result->id], ['class' => 'btn btn-info', 'escape' => false])
    ]);
}
echo $this->DataTables->response();

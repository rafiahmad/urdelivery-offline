<?= $this->Html->component('moment/moment', 'script') ?>
<?= $this->Html->component('bootstrap-daterangepicker/daterangepicker', 'css') ?>
<?= $this->Html->component('bootstrap-daterangepicker/daterangepicker', 'script') ?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">List Orders</h3>
        <div class="pull-right">
            <?= $this->Html->link('<i class="fa fa-undo"></i>', ['action' => 'clearFilterSession'], ['class' => 'btn btn-info', 'title' => 'Reset Filter', 'escape' => false]) ?>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?= $this->Form->create(null, ['id' => 'order-form']); ?>
        <div class="row form-group">
            <div class="col-md-3">
                <?=
                $this->Form->control('current_status', ['options' => [
                        '' => 'Select Current Status',
                        'pickup' => 'Pickup',
                        'delivered' => 'Delivered',
                        'reassign' => 'Re Assign',
                        'cancel' => 'Cancel'
                    ], 'onchange' => "$('#order-form').submit()", 'default' => isset($current_status) ? $current_status : ''])
                ?>
            </div>
            <?php if ($authUser['role'] === 1) { ?>
                <div class="col-md-3">
                    <?=
                    $this->Form->control('client_name', ['options' => $clients, 'empty' => 'Select Client Name',
                        'onchange' => "$('#order-form').submit()", 'default' => isset($client_name) ? $client_name : ''])
                    ?>
                </div>
                <div class="col-md-3">
                    <?=
                    $this->Form->control('rider_name', ['options' => ['null' => 'Un Assign'] + $riders->toArray(), 'empty' => 'Select Rider Name',
                        'onchange' => "$('#order-form').submit()", 'default' => isset($rider_name) ? $rider_name : ''])
                    ?>
                </div>
            <?php } ?>
            <div class="col-md-3">
                <?=
                $this->Form->control('payment_method', ['options' => [
                        '' => 'Select Payment Method',
                        'cash' => 'Cash',
                        'prepaid' => 'Prepaid',
                    ], 'onchange' => "$('#order-form').submit()", 'default' => isset($payment_method) ? $payment_method : ''])
                ?>
            </div>
        </div>
        <?= $this->Form->end() ?>
        <?= $this->DataTables->render('Orders') ?>
    </div>
</div>
<?php if ($authUser['role'] === 1) { ?>
    <div class="modal fade" id="modal-rider">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Select Rider</h4>
                </div>
                <?= $this->Form->create(null, ['url' => ['action' => 'assign-rider']]) ?>
                <div class="modal-body">
                    <?= $this->Form->control('order_number', ['readonly' => true]) ?>
                    <?= $this->Form->control('rider_id', ['label' => 'Select Rider', 'options' => $riders, 'required' => true]) ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <?= $this->Form->button('Set Rider') ?>
                </div>
                <?= $this->Form->end() ?>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
<?php } ?>
<!-- Default box -->
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"><?= __('Edit Order') ?></h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
        </div>
    </div>
    <?= $this->Form->create($order) ?>
    <div class="box-body">
        <?php
        echo $this->Form->control('order_number');
        echo $this->Form->control('payment_method');
        echo $this->Form->control('grand_total');
        echo $this->Form->control('customer_name');
        echo $this->Form->control('shipping_street');
        echo $this->Form->control('pincode');
        echo $this->Form->control('city');
        echo $this->Form->control('state');
        echo $this->Form->control('mobile');
        echo $this->Form->control('handover_date', ['type' => 'text', 'value' => date('Y-m-d', strtotime($order->handover_date)), 'datepicker' => true]);
        echo $this->Form->control('delivery_person');
        echo $this->Form->control('attempt_date', ['type' => 'text', 'value' => date('Y-m-d', strtotime($order->attempt_date)), 'datepicker' => true]);
        echo $this->Form->control('final_status');
        echo $this->Form->control('delivery_date', ['type' => 'text', 'value' => date('Y-m-d', strtotime($order->delivery_date)), 'datepicker' => true]);
        echo $this->Form->control('remarks');
        echo $this->Form->control('chargeable_weight');
        ?>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        <?= $this->Form->button(__('Update')) ?>
    </div>
    <?= $this->Form->end() ?>
    <!-- /.box-footer-->
</div>
<!-- /.box -->

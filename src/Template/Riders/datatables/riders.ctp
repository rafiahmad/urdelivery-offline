<?php

foreach ($results as $result) {
    $this->DataTables->prepareData([
        h($result->name),
        h($result->email),
        h($result->mobile),
        $result->status ? 'Active' : 'Inactive',
        $this->Form->postLink('<i class="fa fa-dot-circle-o"></i>', ['action' => 'active', $result->id, ($result->status == '1' ? '0' : '1')], ['confirm' => __('Are you sure you want to ' . ($result->status == '1' ? 'Deactivate' : 'Activate') . ' # {0}?', $result->name), 'escape' => false, 'class' => 'btn btn-info']) . '&nbsp;' .
        $this->Html->link(__('<i class="fa fa-pencil"></i>'), ['action' => 'edit', $result->id], ['class' => 'btn btn-info', 'escape' => false])
    ]);
}
echo $this->DataTables->response();

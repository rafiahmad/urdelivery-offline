<?php
$this->assign('title', 'Riders');
?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">List Riders</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?= $this->DataTables->render('Riders') ?>
    </div>
</div>
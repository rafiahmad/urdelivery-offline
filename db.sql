/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Shelty
 * Created: 3 Aug, 2020
 */


--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `rider_id` int(11) DEFAULT NULL,
  `order_number` varchar(20) NOT NULL,
  `payment_method` varchar(10) NOT NULL,
  `customer_name` varchar(100) NOT NULL,
  `shipping_street` varchar(750) NOT NULL,
  `pincode` varchar(10) NOT NULL,
  `city` varchar(20) NOT NULL,
  `state` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `status` varchar(20) DEFAULT NULL,
  `pickup_date` datetime DEFAULT NULL,
  `delivery_date` datetime DEFAULT NULL,
  `cancel_date` datetime DEFAULT NULL,
  `reassign_date` datetime DEFAULT NULL,
  `remarks` longtext,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `oder_id` (`order_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `orders`
--

TRUNCATE TABLE `orders`;
-- --------------------------------------------------------

--
-- Table structure for table `tokens`
--

DROP TABLE IF EXISTS `tokens`;
CREATE TABLE IF NOT EXISTS `tokens` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `token` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `tokens`
--

TRUNCATE TABLE `tokens`;
-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` tinyint(4) NOT NULL DEFAULT '2' COMMENT '1 => admin , 2 => client, 3 => rider',
  `name` varchar(50) DEFAULT NULL,
  `street_address` text,
  `pincode` varchar(10) DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0 => inactive , 1 => active',
  `auth_token` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `token_expire` datetime DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `users`
--

TRUNCATE TABLE `users`;
--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role`, `name`, `street_address`, `pincode`, `city`, `state`, `mobile`, `email`, `password`, `status`, `auth_token`, `token`, `token_expire`, `created`, `modified`) VALUES
(1, 1, 'Admin User', 'C-15, Jhankar Road, Goyla Dairy, Qutub Vihar', '110071', 'Delhi', 'New Delhi', '7210482353', 'admin@gmail.com', '$2y$10$/o0cn7Rt8i/nyENGnPuTSeRbOuRwPPo4snOWm2XlSU2gXzYTDptka', 1, NULL, NULL, NULL, '2020-07-30 22:45:36', NULL);
COMMIT;